# Storage output
output "sftp_storage_name" {
  value = azurerm_storage_account.stacc_sftp.name
}

output "sftp_storage_id" {
  value = azurerm_storage_account.stacc_sftp.id
}

output "sftp_storage_access_key" {
  value = azurerm_storage_account.stacc_sftp.primary_access_key
}


# Output vm

output "vm_ip" {
  value = azurerm_network_interface.nic_private.private_ip_address
}

output "vm_nic_id" {
  value = azurerm_network_interface.nic_private.id
}

output "vm_id" {
  value = azurerm_virtual_machine.vm_sftp.id
}

output "vm_name" {
  value = azurerm_virtual_machine.vm_sftp.name
}



