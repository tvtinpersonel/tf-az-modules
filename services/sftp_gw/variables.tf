variable "location" {  
}
variable "resource_group_name" {
}

variable "env" {
}

variable "storage_account_name" {
  default = "sftgatewaysttacc"
}


variable "account_tier" {
  default = "Standard"
}

variable "account_replication_type" {
  default = "LRS"
}

variable "sftp_vm_name" {
}


# Var for VM gateway

variable network_security_group_id {
}

variable "subnet_id" { 
}

variable "private_ip_address_allocation" {
  default = "Dynamic"
}

variable "vm_size" {
  default = "B2ms"
}

variable "delete_os_disk_on_termination" {
  default = "true"
}

variable "os_disk_type" {
    default = "Standard_LRS" 
}

variable "os_disk_size" {
    default = "100"
}

# data disk_size
variable "data_disk_option" {
  description = "Specify add data disk or not"
  default = true
}

variable "data_disk_size" {
  description = "Specify the size in GB of the data disk"
  default = "10"
}

variable "data_disk_type" {
  description = "Possible values are either Standard_LRS, StandardSSD_LRS, Premium_LRS or UltraSSD_LRS"
  default = "Standard_LRS"
}

variable "os_admin_user" {
  default = "cloud-user"
}

variable "ssh_key_data" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfYhaId03HajWYkogxNqUUcRctU/jmfpn6sdqIiLARPsZgAbgQQFxl/jrkjufhEMl+qwSSeekGwZxCscfqn/artV5B8B9uF5H+jyUpvw/drWJZQpk2hsEY6kVH+qJtxH2FQ1dwOhVxP87bK8dmoaupf6PMb3dZ7+I9d0+fG0BpOoqBdKH7xc+3LZ5KZfPgc3u1dnPFU6/dWY08Inln1BiW8UhJBiX+yKXZjy+al2vjIgeXoLY03IgWTcGbF8WEl7VIDYPM6oDv+QubV3uoYaMpnEXI+g6kRqPtSdc2XUAvDq4Z2LhSbg2vJDZX8jsRwgG3C3nvWMwlKWSVCDkhwqOP tin@cc-4a9b6aaa-7979b569d5-2s2t"
}

variable "user_data" {
    default = "sftp_setup.tpl"
}

variable "module_depends_on" {
    type = list
    default = []
}

variable "public_vm" {
   description = "Public VM or not. true/false"
   type = bool
   default = true
}

variable "public_ip_id" {
  default = null
}

variable "sftp_admin_pass" {
}
