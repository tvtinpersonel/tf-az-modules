
data "template_file" "user_data" {
  template = "${file("${path.module}/../templates/${var.user_data}")}"
  vars = {
    storage_account = azurerm_storage_account.stacc_sftp.name
    storage_key = azurerm_storage_account.stacc_sftp.primary_access_key
    sftp_admin_pass = var.sftp_admin_pass
  }
}

data "template_cloudinit_config" "config"{
gzip = false
base64_encode = false

part {
  content_type = "text/cloud-config"
  content = data.template_file.user_data.rendered
  }
}
