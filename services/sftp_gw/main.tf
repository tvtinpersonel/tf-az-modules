resource "azurerm_storage_account" "stacc_sftp" {
  name                = var.storage_account_name
  resource_group_name = var.resource_group_name

  account_kind             = "BlobStorage"
  location                 = var.location
  account_tier             = var.account_tier
  account_replication_type = var.account_replication_type

  tags = {
    env = var.env
  }

}

resource "azurerm_network_interface" "nic_private" {

  name                = "${var.sftp_vm_name}-nic"
  location            = var.location
  resource_group_name = var.resource_group_name
  network_security_group_id = var.network_security_group_id

# Block ip configurations for public IP
dynamic "ip_configuration" {
  for_each = var.public_vm == true ? [var.public_vm] : []
  content {
    name                          = "${var.sftp_vm_name}-nic"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = var.private_ip_address_allocation
    public_ip_address_id          = var.public_ip_id
  }
}

# Block ip configurations for no public_ip
dynamic "ip_configuration" {
  for_each = var.public_vm == false ? [var.public_vm] : []
  content {
    name                          = "${var.sftp_vm_name}-nic"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = var.private_ip_address_allocation
  }
}

}

resource "azurerm_virtual_machine" "vm_sftp" {
  name                  = var.sftp_vm_name
  location              = var.location
  resource_group_name = var.resource_group_name
  network_interface_ids = [azurerm_network_interface.nic_private.id]
  vm_size               = var.vm_size

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = var.delete_os_disk_on_termination
  
  storage_image_reference {
    publisher = "thorntechnologiesllc"
    offer     = "sftpgateway"
    sku       = "sftpgateway"
    version   = "2.000.02"
  }

  plan {
    publisher = "thorntechnologiesllc"
    name     = "sftpgateway"
    product  = "sftpgateway"
    #version   = "2.000.02"
  }
  storage_os_disk {
    name              = "${var.sftp_vm_name}-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = var.os_disk_type
    disk_size_gb      = var.os_disk_size
  }

  dynamic "storage_data_disk" {
    for_each = var.data_disk_option == true ? [var.data_disk_option] : []
    content {
      name = "${var.sftp_vm_name}-data-disk"
      lun           = 0
      caching       = "ReadWrite"
      create_option = "Empty"
      disk_size_gb  = var.data_disk_size
      managed_disk_type = var.data_disk_type
  }
  }

  os_profile {
    computer_name  = var.sftp_vm_name
    admin_username = var.os_admin_user
    custom_data    = data.template_cloudinit_config.config.rendered
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.os_admin_user}/.ssh/authorized_keys"
      key_data = var.ssh_key_data
    }


  }
  tags = {
    env = var.env
    service = "sftp"
  }
}
