#cloud-config
manage_etc_hosts: true
locale: "en_US.UTF-8"
resolv_conf:
  nameservers: ['8.8.4.4', '8.8.8.8']
  options:
    rotate: true
    timeout: 1
package_update: true
#apt_upgrade: true
packages:
 - nano
 - unzip
 - epel-release
 - python-pip 
write_files:
-   content: |
        Storage key is ${storage_key}
    path: /var/log/terraform-init
# Mouting /dev/sdc to /home
disk_setup:
  /dev/sdc:
    table_type: gpt
    layout: True
    overwrite: True
fs_setup:
  - device: /dev/sdc
    partition:  1
    filesystem: ext4
mounts:
  - [ /dev/sdc1, /home, "ext4", "defaults,nofail", "1", "2" ]
# SFTP Installation
runcmd:
  # Setup storage
  - sudo /usr/local/bin/storage-account-setup  --account-name "${storage_account}" --account-key "${storage_key}" >> /var/log/terraform-init
  # Change password
  - sudo /usr/local/bin/resetadminpassword -p "${sftp_admin_pass}" >> /var/log/terraform-init
  # sftp backup
runcmd: 
  - pip install PyYaml >> /var/log/terraform-init
  - mkdir /home/backup && chmod -R 700 /home/backup
  - wget "https://s3.amazonaws.com/thorntech-public-documents/sftpgateway/backup-and-recovery/sftpgw-backup.py" -O /home/backup/sftpgw-backup.py
  - mkdir /home/backup/sftp-backup-data
write_files:
  - owner: root:root
    path: /etc/cron.d/sftp_backup
    content: | 
      00 01 * * * python /home/backup/sftpgw-backup.py --destination /home/sftp_backup_data
#SFTP custom
runcmd:
  - sed -i "s@^usersetup.*@#usersetup $user@g" /usr/local/bin/pamwrapper.sh
write_files:
  - owner: root:root
    path: /opt/sftpgw/set_user.sh
    content: |
      #!/bin/bash
      for DIR in /home/*/home/*;do
        USER=$(basename $DIR)
      # Creating working dir if does not exist
      if [ ! -d "$DIR/$USER"  ];then
        mkdir -p $DIR/$USER
        chown -R $USER:$USER $DIR/$USER
        chmod -R 750 $DIR/$USER
      fi
      if [ -d "$DIR/uploads"  ];then
        rm -rf $DIR/uploads
      fi
      if [ -d "$DIR/local"];then
        rm -rf $DIR/local
      fi
      if [ -d "$DIR/downloads" ];then
        rm -rf $DIR/downloads
      fi
      BASEDIR=$(dirname "$0")
      bash $BASEDIR/sharing-user-sync.sh $# $USER
      done
write_files:
  - owner: root:root
    path: /opt/sftpgw/sharing-user-sync.sh
    content: |
      #!/bin/bash
      LOG_FILE="/var/log/sftpgw/syncfromblob.log"
      function applicationprop {
        local prefix="${1}="
        local str=$(grep "${1}" /opt/sftpgw/application.properties 2>/dev/null)
        echo ${str#${prefix}}
      }
      function sftpgwprop {
        local prefix="${1}="
        local str=$(grep "${1}" /opt/sftpgw/sftpgateway.properties 2>/dev/null)
        echo ${str#${prefix}}
      }
      azure_storage_account="$(applicationprop 'azure.storage.account-name')"
      azure_storage_key="$(applicationprop 'azure.storage.account-key')
      # This pulls the default storage location from the sftpgateway.properties file.
      # If you have set a custom storage location for a user then you may have to pull the
      # container from the user.properties file or hardcode it here.
      container_name="$(sftpgwprop 'sftpgateway.bucketname')"
      file=$1
      user=$2
      #echo "az storage blob delete --output json --account-name ${azure_storage_account} --account-key ${azure_storage_key} --container-name $container_name --name $user/downloads/$file &>> $LOG_FILE"
      echo "Sync from /home/$user/home/$user/$user -d $user/$user"  &>> $LOG_FILE
      az storage blob sync --output json --account-name ${azure_storage_account} --account-key ${azure_storage_key} --container $container_name -s "/home/$user/home/$user/$user" -d "$user/$user" &>> $LOG_FILE
      # end of script
write_files:
  - owner: root:root
    path: /etc/cron.d/customize_user
    content: | 
      * * * * * bash /opt/sftpgw/set_user.sh