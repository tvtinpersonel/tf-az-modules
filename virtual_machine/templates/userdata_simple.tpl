#cloud-config
manage_etc_hosts: true
locale: "en_US.UTF-8"
resolv_conf:
  nameservers: ['8.8.4.4', '8.8.8.8']
  options:
    rotate: true
    timeout: 1
package_update: true
package_upgrade: true
packages:
 - nano
 - unzip
 - epel-release
write_files:
-   content: |
        The installation has been success
    path: /var/log/terraform-init
runcmd:
  - yum install -y python-pip
  - sudo pip install ansible==2.4.1.0
  - sudo ansible-galaxy install azure.azure_preview_modules
  - sudo pip install -r ~/.ansible/roles/azure.azure_preview_modules/files/requirements-azure.txt