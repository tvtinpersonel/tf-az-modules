output "vm_data_ip" {
  value = azurerm_network_interface.nic_private.private_ip_address
}

output "vn_data_nic_id" {
  value = azurerm_network_interface.nic_private.id
}

output "vn_data_id" {
  value = azurerm_virtual_machine.vm_data.id
}

#output "public_ip_id" {
#  value = "${element(concat(azurerm_public_ip.public_ip.*.id, list("")), 0)}"
#}


#output "public_ip" {
#  value = "${element(concat(azurerm_public_ip.public_ip.*.ip_address, list("")), 0)}"
#}


