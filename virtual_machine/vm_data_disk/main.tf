resource "azurerm_network_interface" "nic_private" {

  name                = "${var.vm_name}-nic"
  location            = var.location
  resource_group_name = var.resource_group_name
  network_security_group_id = var.network_security_group_id

# Block ip configurations for public IP
dynamic "ip_configuration" {
  for_each = var.public_vm == true ? [var.public_vm] : []
  content {
    name                          = "${var.vm_name}-pip"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = var.private_ip_address_allocation
    public_ip_address_id          = var.public_ip_id
    load_balancer_backend_address_pools_ids = var.lb_backend_id
  }
}

# Block ip configurations for no public_ip
dynamic "ip_configuration" {
  for_each = var.public_vm == false ? [var.public_vm] : []
  content {
    name                          = "${var.vm_name}-nic"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = var.private_ip_address_allocation
    load_balancer_backend_address_pools_ids = var.lb_backend_id
  }
}

}

resource "azurerm_virtual_machine" "vm_data" {
  name                  = var.vm_name
  location              = var.location
  resource_group_name = var.resource_group_name
  network_interface_ids = [azurerm_network_interface.nic_private.id]
  vm_size               = var.vm_size

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = var.delete_os_disk_on_termination

  storage_image_reference {
    publisher = var.os_publisher
    offer     = var.os_offer
    sku       = var.os_sku
    version   = var.os_version
  }
  storage_os_disk {
    name              = "${var.vm_name}-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    disk_size_gb      = var.os_disk_size
    managed_disk_type = var.os_disk_type
  }

  # Add more storage for data disk 
  delete_data_disks_on_termination = var.delete_data_disks_on_termination

  dynamic "storage_data_disk" {
    for_each = var.data_disk_option == true ? [var.data_disk_option] : []
    content {
      name = "${var.vm_name}-data-disk"
      lun           = 0
      caching       = "ReadWrite"
      create_option = "Empty"
      disk_size_gb  = var.data_disk_size
      managed_disk_type = var.data_disk_type
  }
  }

  os_profile {
    computer_name  = var.vm_name
    admin_username = var.os_admin_user
    custom_data    = data.template_cloudinit_config.config.rendered
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.os_admin_user}/.ssh/authorized_keys"
      key_data = var.ssh_key_data
    }

  }
  tags = {
    env = var.env
  }
}