
data "template_file" "user_data" {
  template = "${file("${path.module}/../templates/${var.user_data}")}"
}

data "template_cloudinit_config" "config"{
gzip = false
base64_encode = false

part {
  content_type = "text/cloud-config"
  content = data.template_file.user_data.rendered
  }
}
