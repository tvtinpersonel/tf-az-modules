variable "env" {
  }

variable "vm_name" {  
}

variable "location" {  
}

variable "resource_group_name" {
}

variable network_security_group_id {
  type = string
  default = null
}

variable "lb_backend_id" {
  type = list
  default = []
}


variable "subnet_id" { 
}

variable "private_ip_address_allocation" {
  default = "Dynamic"
}

variable "vm_size" {
}

variable "delete_os_disk_on_termination" {
  default = "true"
}

variable "delete_data_disks_on_termination" {
  default = "true"
}

variable "public_ip_id" {
  type = string
  default = null
}

# data disk_size
variable "data_disk_option" {
  description = "Specify add data disk or not"
  default = true
}

variable "data_disk_size" {
  description = "Specify the size in GB of the data disk"
  default = "10"
}

variable "data_disk_type" {
  description = "Possible values are either Standard_LRS, StandardSSD_LRS, Premium_LRS or UltraSSD_LRS"
  default = "Standard_LRS"
}


# Using az vm image list in Azure CLI to show the informations
variable "os_publisher" {
  default = "OpenLogic"
}

variable "os_offer" {
  default = "CentOS"
}

variable "os_sku" {
  default = "7.7"
}

variable "os_version" {
  default = "latest"
}

variable "os_disk_type" {
    default = "Standard_LRS" 
}

variable "os_disk_size" {
    default = "30"
}


variable "os_admin_user" {
  default = "cloud-user"
}

variable "ssh_key_data" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfYhaId03HajWYkogxNqUUcRctU/jmfpn6sdqIiLARPsZgAbgQQFxl/jrkjufhEMl+qwSSeekGwZxCscfqn/artV5B8B9uF5H+jyUpvw/drWJZQpk2hsEY6kVH+qJtxH2FQ1dwOhVxP87bK8dmoaupf6PMb3dZ7+I9d0+fG0BpOoqBdKH7xc+3LZ5KZfPgc3u1dnPFU6/dWY08Inln1BiW8UhJBiX+yKXZjy+al2vjIgeXoLY03IgWTcGbF8WEl7VIDYPM6oDv+QubV3uoYaMpnEXI+g6kRqPtSdc2XUAvDq4Z2LhSbg2vJDZX8jsRwgG3C3nvWMwlKWSVCDkhwqOP tin@cc-4a9b6aaa-7979b569d5-2s2t"
}

variable "user_data" {
    default = "userdata_simple.tpl"
}

variable "module_depends_on" {
    type = list
    default = []
}

variable "public_vm" {
   description = "Public VM or not. true/false"
   type = bool
   default = true
}
















