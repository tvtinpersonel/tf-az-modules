# VN output
output "vn_id" {
  value = azurerm_virtual_network.vn.id
}
output "vn_name" {
  value = azurerm_virtual_network.vn.name
}

# Subnet output
output "subnet_name" {
  value = azurerm_subnet.subnet.*.name
}
output "subnet_id" {
  value = azurerm_subnet.subnet.*.id
}
output "subnet_cidr" {
  value = azurerm_subnet.subnet.*.address_prefix
}
output "subnet_ip_conf" {
  value = azurerm_subnet.subnet.*.ip_configurations
}



