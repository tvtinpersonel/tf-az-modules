# General Variables
variable "env" {
  description = "Environment"
}

variable "resource_group_name" {
}


# vn variables
variable "vn_name" {
}

variable "vn_cidr" {
  description = "vn address space"
}
variable "location" {
  }

# subent variables
variable "subnet_name" {
  type        = list(string)
  description = "List of subnet name. Eg subnet_name = [internal,external,vpn]"
}
variable "subnet_newbits" {
  description = "The new mask for the subnet within the virtual network, and. For example, subnet_newbits = 8 . cidrsubnet(10.0.0.0/8, 8, 2) returns 10.2.0.0/16"
}
variable "subnet_number" {
  type        = map(string)
  description = "The zero-based index of the subnet when the network is masked with the newbit.. For example, sub_number = 2 . cidrsubnet(10.0.0.0/8, 8, 2) returns 10.2.0.0/16"
}


# public_ip variable

variable "allocation_method" {
  default = "Static"
  description = "Static or Dynamic for public ip"
}
