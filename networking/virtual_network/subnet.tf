resource "azurerm_subnet" "subnet" {
  count = length(var.subnet_name)
  depends_on = [azurerm_virtual_network.vn]
  
  name = element(var.subnet_name, count.index )
  address_prefix = cidrsubnet(var.vn_cidr,var.subnet_newbits ,lookup(var.subnet_number, element(var.subnet_name,count.index)) )
  virtual_network_name = azurerm_virtual_network.vn.name
  resource_group_name = var.resource_group_name
}
