resource "azurerm_virtual_network" "vn" {
    name    = var.vn_name
    address_space = ["${var.vn_cidr}"]
    location = var.location
    resource_group_name = var.resource_group_name
    tags = {
        env = var.env
        type = "virtual_network"
    }
}