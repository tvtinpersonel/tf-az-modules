resource "azurerm_resource_group" "rg" {
  name = "rg-${var.env}-${var.project}-${var.service}"
  location = var.location
}
